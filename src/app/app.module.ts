import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

// pages
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { OthersComponent } from './pages/others/others.component';

// routes
import { RouterModule, Routes } from '@angular/router';
const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'others', component: OthersComponent }
];

@NgModule({
  declarations: [
    // pages & components
      AppComponent,
      HomeComponent,
      OthersComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // routes
      RouterModule.forRoot(appRoutes),
    // firebase
      AngularFireModule.initializeApp(environment.firebase),
      AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
