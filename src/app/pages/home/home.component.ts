import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import { Account } from '../../classes/Account';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  accounts: FirebaseListObservable< Account[] >;
  // account: FirebaseListObservable< Account >;
  varAddAccount: any = {};
  varEditAccount: any = {};

  showAddMenu = false;
  showEditMenu = false;

  constructor(private db: AngularFireDatabase) {
  }

  ngOnInit() {
    this.getAccounts();
  }

  getAccounts() {
    this.accounts = this.db.list('/accounts');
  }

  openAddMenu() {this.showAddMenu = true; this.showEditMenu = false;}
  closeAddMenu() {this.showAddMenu = false; }
  addAccount(varAddAccount: any): void {
    this.accounts.push(varAddAccount);
  }

  openEditMenu() {this.showEditMenu = true; this.showAddMenu = false; }
  closeEditMenu() {this.showEditMenu = false; this.getAccounts(); }
  choseEditAccount(account: any): void {
    this.varEditAccount = account;
  }
  saveEditAccount(varEditAccount: any): void {
    this.db.object('/accounts/' + varEditAccount.$key).update(varEditAccount);
  }

  deleteAccount(account: any): void {
    this.db.object('/accounts/' + account.$key).remove();
  }

}
